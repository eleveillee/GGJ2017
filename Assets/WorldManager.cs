﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Random = UnityEngine.Random;

public class WorldManager : MonoBehaviour
{
    [SerializeField] private Token _tokenPrefab;
    [SerializeField] private List<GameObject> _hazards;
    [SerializeField] private GameObject _miscObjects;

    private List<GameObject> _instantiatedObjects = new List<GameObject>();

    private float _maxX = 18f;
    private float _maxY = 9f;

    public static WorldManager Instance;
    private int _maxObjects = 10;
    private float _spawnChance = 0.005f;

    private Token _tokenInstance;
    private bool _isStarted;
    // Use this for initialization
	void Awake ()
	{
	    if (Instance == null)
	        Instance = this;
	    else
	        Destroy(this);
	}
	

	public void StartWorld()
	{
	    SpawnNewToken();
	    _isStarted = true;
	}

    public void TokenDestroyed()
    {
        LeanTween.delayedCall(Random.Range(0f, 5f), () => { SpawnNewToken(); });
    }

    void SpawnNewToken()
    {
        Vector3 randomPosition = new Vector3(Random.Range(-_maxX, _maxX), Random.Range(-_maxY, _maxY), 0f);
        _tokenInstance = ((Token) Instantiate(_tokenPrefab, randomPosition, Quaternion.identity, _miscObjects.transform)
            .GetComponent<Token>());
    }

    void Update()
    {
        if (!_isStarted)
            return;

        if(Random.Range(0f, 1f) < _spawnChance) //(_instantiatedObjects.Count < _maxObjects)
        {
            SpawnHazard(_hazards.GetRandomValue());
        }
    }

    void SpawnHazard(GameObject go)
    {
        float factorDistance = 3f;
        float _randomX = Random.Range(-_maxX * factorDistance, _maxX * factorDistance);
        float randomY = 0f;
        if (_randomX < _maxX || _randomX > -_maxX) // if X is horizontal to the playzone
        {
            randomY = Random.Range(_maxY, _maxY * factorDistance);
            randomY *= Random.Range(0f, 1f) > 0.5f ? 1f : -1f;
        }
        else
        {
            randomY = Random.Range(_maxY*factorDistance, -_maxY * factorDistance);
        }

        Vector3 newPosition = new Vector3(_randomX, randomY, 0f);
        GameObject newHazard = (GameObject) Instantiate(go, newPosition, Quaternion.identity, _miscObjects.transform);
        //Debug.Log("newHazard = " + newPosition);
        Rigidbody2D rb2d = newHazard.GetComponent<Rigidbody2D>();
        if (rb2d == null)
        {
            Destroy(newHazard);
            Debug.LogWarning("Missing Rigibody on hazard");
            return;
        }

        Vector3 target = Vector3.zero + new Vector3(Random.Range(_maxX * 0.6f, -_maxX * 0.6f), Random.Range(_maxY * 0.6f, -_maxY * 0.6f), 0f);

        rb2d.AddForce(Random.Range(100f, 350f) * (target - newHazard.transform.position).normalized);
        rb2d.AddTorque(Random.Range(100f, 250f));

        _instantiatedObjects.Add(newHazard);
    }

    public void StopWorld()
    {
        _isStarted = false;
    }
}
