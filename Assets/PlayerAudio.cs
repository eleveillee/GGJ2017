﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAudio : MonoBehaviour {

    [SerializeField]
    private AudioSource _fxSource;
    [SerializeField]
    private AudioSource _loopSource;

    [SerializeField]
    private GameObject _detachedSource;
    private AudioSource _source;  

    [SerializeField]
    private AudioClip _wave;
    [SerializeField]
    private AudioClip _die;
    [SerializeField]
    private AudioClip _token;
    [SerializeField]
    private AudioClip _boost;

    public void WaveAudio(bool isFiring, float intensity)
    {
        if (!isFiring)
        {
            _loopSource.clip = null;
            _loopSource.loop = false;
            _loopSource.Stop();
            return;
        }

        _loopSource.clip = _wave;
        _loopSource.loop = true;
        _loopSource.volume = 0.5f;
        _loopSource.Play();
    }

    public void BoostAudio()
    {
        _fxSource.clip = _boost;
        _fxSource.pitch = .3f;
        _fxSource.volume = .35f;
        _fxSource.Play();
    }

    public void TokenAudio()
    {
        _fxSource.clip = _token;
        _fxSource.pitch = 2f;
        _fxSource.volume = .4f;
        _fxSource.Play();        
    }

    public void DieAudio()
    {        
        if (_source == null)
        {
            GameObject ds = Instantiate(_detachedSource);                  
            _source = ds.GetComponent<AudioSource>();
            _source.pitch = .3f;
            _source.PlayOneShot(_die, .4f);
        }

        else
        {
            _source.transform.position = transform.position;
            _source.PlayOneShot(_die, .4f);
        }
    }

    public AudioSource AudioSource
    {
        get { return _loopSource; }
        set { _loopSource = value; }
    }
}
