﻿using System;
using UnityEngine;
using System.Collections;

public class FSM_GameState : FSM.FSM
{
    public static FSM_GameState Instance;

    [Header("States")]
    public GameState_Bootstrap State_Boostrap;
    public GameState_MainMenu State_MainMenu;
    public GameState_Game State_Game;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);
    }
}


