using System;
using UnityEngine;


[Serializable]
public class GameState_MainMenu : FSM.State
{
    private UIPanelMainMenu _uiPanelMainMenu;

    protected override void OnEnter()
    {
        _uiPanelMainMenu = (UIPanelMainMenu)UIManager.Instance.ShowPanel(PanelType.MainMenu);
        UIManager.Instance.FadeIn(1f);
    }

    protected override void OnExit()
    {
        UIManager.Instance.FadeOut(0.5f, HidePanel);
    }

    void HidePanel()
    {
        UIManager.Instance.ShowPanel(PanelType.MainMenu, false);
    }
}


