﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class GameState_Game : FSM.State
{
    private bool _isPaused;

    protected override void OnEnter()
    {
        PauseGame(false);

        UIManager.Instance.FadeOut(0f);
        UIManager.Instance.ShowUIOVerlay();
        UIManager.Instance.FadeIn(2f, CallEnterGameMode);
    }

    void CallEnterGameMode()
    {
        FSM_FightState.Instance.ChangeState(FSM_FightState.Instance.Bootstrap);
    }

    protected override void OnUpdate()
    {
        //Debug.Log("Scale : " + Time.timeScale);
        if (LevelManager.Instance.IsPlayerPressingStart())
            PauseGame(!_isPaused);

    }

    void PauseGame(bool isPaused)
    {
        _isPaused = isPaused;
        Time.timeScale = isPaused ? 0f : 1f;
        UIManager.Instance.ShowPanel(PanelType.Pause, isPaused);
    }

    protected override void OnExit()
    {
        UIManager.Instance.ShowUIOVerlay(false);
        LevelManager.Instance.ExitGameMode();
        UIManager.Instance.FadeOut(0.5f);
    }
}


