using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[Serializable]
public class GameState_Bootstrap : FSM.State
{
    [SerializeField]
    private GameObject _splashScreen;

    protected override void OnEnter()
    {
        UIManager.Instance.FadeOut(0f);
        LeanTween.init(800);
        GameManager.Instance.InitializePlayerInstances();
        GameManager.Instance.StartCoroutine(DisplaySplashScreen());
    }

    IEnumerator DisplaySplashScreen()
    {
        yield return new WaitForSeconds(0.5f);
        _splashScreen.gameObject.SetActive(true);
        UIManager.Instance.FadeIn(1f);
        yield return new WaitForSeconds(3f);
        UIManager.Instance.FadeOut(0.5f);
        yield return new WaitForSeconds(1f);
        FSM_GameState.Instance.ChangeState(FSM_GameState.Instance.State_MainMenu);
    }

    protected override void OnExit()
    {
        _splashScreen.SetActive(false);
    }
}


