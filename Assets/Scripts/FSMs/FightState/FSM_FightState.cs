﻿using System;
using UnityEngine;
using System.Collections;

public class FSM_FightState : FSM.FSM
{
    public static FSM_FightState Instance;

    [Header("States")]
    public FightState_Bootstrap Bootstrap;
    public FightState_InProgress InProgress;
    public FightState_MidGame MidGame;
    public FightState_EndGame EndGame;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);
    }
}


