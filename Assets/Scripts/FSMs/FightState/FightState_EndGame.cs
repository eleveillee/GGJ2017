using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[Serializable]
public class FightState_EndGame : FSM.State
{
    [SerializeField]
    private GameObject _splashScreen;

    protected override void OnEnter()
    {
        UIManager.Instance.ShowPanel(PanelType.EndGame);
    }

    protected override void OnExit()
    {
        UIManager.Instance.ShowPanel(PanelType.EndGame, false);
    }
}


