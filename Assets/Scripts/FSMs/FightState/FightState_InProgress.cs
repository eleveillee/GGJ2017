using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[Serializable]
public class FightState_InProgress : FSM.State
{
    protected override void OnEnter()
    {
        LevelManager.Instance.StartFight();
    }

    protected override void OnExit()
    {

    }
}


