using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[Serializable]
public class FightState_Bootstrap : FSM.State
{
    protected override void OnEnter()
    {
        LevelManager.Instance.EnterGameMode();
        _fsm.ChangeState(FSM_FightState.Instance.InProgress);
    }

    protected override void OnExit()
    {

    }
}


