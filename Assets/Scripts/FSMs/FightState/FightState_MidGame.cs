using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[Serializable]
public class FightState_MidGame : FSM.State
{
    [SerializeField]
    private GameObject _splashScreen;

    protected override void OnEnter()
    {
        LevelManager.Instance.TogglePlayerControllers(false);
        UIManager.Instance.ShowPanel(PanelType.MidGame);
    }

    protected override void OnExit()
    {
        UIManager.Instance.ShowPanel(PanelType.MidGame, false);
    }
}


