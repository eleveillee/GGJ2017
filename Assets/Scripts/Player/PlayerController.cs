﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Versioning;
using System.Security.Cryptography.X509Certificates;
using UnityEditor;
using UnityEngine;
using XInputDotNetPure;

public class PlayerController : MonoBehaviour
{

    [SerializeField] private GameObject _model;
    [SerializeField] private GameObject _gunRay;
    [SerializeField] private ParticleSystem _gunRayParticle;
                     private ParticleSystem.Particle[] _particles;
    [SerializeField] private PolygonCollider2D _beamCollider;    
    [SerializeField] private ParticleSystem _boostParticle;
    
    private Rigidbody2D _rigidbody2D;
    private GamepadDevice _gamepad;

    public bool CanControl = false;
    private Vector3 _initialPosition;

    // Translation
    private bool _isBoosting;
    private float _forwardForce = 100f;
    private float _lateralForce = 50f;
    private float _boostForce   = 50f; // Impulsion
    private float _maxSpeed = 6f;
    private float _maxSpeedBoosting = 12f;
    private float _delayBoosting = 3f;
    private float _timeBoost;

    // Rotation
    private float _rotationSpeed = 5f;

    // Shooting
    private bool _isShooting;
    private float _delayShooting = 5f;
    private float _timeShoot;
    private float _projectileForce = 2000f;

    // Switching
    private bool _isSwitching;
    private float _timeSwitch = 0.3f;

    // PArticle
    public float Amplitude = 2f;
    public float Freq = 3f;
    public float yPosMultiplier = 0.5f;
    public float expAmplitude = 0.35f;
    public float minIntensity = 0.3f;
    public float maxIntensity = 0.95f;

    private bool _isReadyToBoost;

    private Player _player;

    private Dictionary<ParticleSystem.Particle, float> _particleStartTime = new Dictionary<ParticleSystem.Particle, float>();

    void Awake()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _player = GetComponent<Player>();
    }

    void Update ()
    {
        if (_player.Id >= GamepadInput.Instance.gamepads.Count)
            return;

        if(_gamepad == null)
            _gamepad = GamepadInput.Instance.gamepads[_player.Id];

        if (_gamepad == null)
	    {
	        Debug.LogWarning("Null GamePad");
	        return;
	    }

	    HandleControl();
        if (_isSwitching)
            return;

        HandleShooting();
        HandleSwitching();
    }

    public void Reset()
    {
        if (_player.IsParticleMode)
            Switch(true);

        _rigidbody2D.velocity = Vector2.zero;
        transform.localPosition = _initialPosition;
    }

    public void Initialize()
    {
        _initialPosition = transform.localPosition;
    }

    void HandleControl()
    {
        Vector2 _leftJoystick = new Vector2(GetAxis(0), GetAxis(1));
        Vector2 _rightJoystick = new Vector2(GetAxis(2), GetAxis(3));

        // Add translation movement
        _rigidbody2D.AddForce(new Vector2(_leftJoystick.x * _lateralForce, _leftJoystick.y * _forwardForce));
        //_rigidbody2D.AddRelativeForce(new Vector2(0f, _leftJoystick.y * _forwardForce)); // new Vector2(_leftJoystick.x * _lateralForce, _leftJoystick.y * _forwardForce));

        // Add Rotation
        if (_rightJoystick.x != 0f || _rightJoystick.y != 0f)
        {

            Vector3 newVector = Vector3.RotateTowards(transform.up, _rightJoystick.normalized, _rotationSpeed * Time.deltaTime, 1f);

            Quaternion rotation = new Quaternion();
            rotation.SetLookRotation(Vector3.forward, newVector);
            transform.rotation = rotation;
        }
    }

    void Boost()
    {
        if (Time.realtimeSinceStartup >= _timeBoost + _delayBoosting)
        {
            Debug.Log("StartBoost");
            _isBoosting = true;
            _player.Audio.BoostAudio();
            StartCoroutine(StopBoost(2f));
            Vector2 _leftJoystick = new Vector2(GetAxis(0), GetAxis(1));
            //Debug.Log("Boost!");
            //_rigidbody2D.AddForce(_leftJoystick.normalized * _boostForce, ForceMode2D.Impulse);
            _isReadyToBoost = true;
            _timeBoost = Time.realtimeSinceStartup;
        }
    }

    IEnumerator StopBoost(float time)
    {
        Debug.Log("StopBoost");
        yield return new WaitForSeconds(time);
        _isBoosting = false;
    }

    void FixedUpdate()
    {
        if (_rigidbody2D.velocity.magnitude > (_isBoosting ? _maxSpeedBoosting : _maxSpeed))
            _rigidbody2D.velocity = _rigidbody2D.velocity.normalized * _maxSpeed;

        if (_isReadyToBoost)
        {
            _rigidbody2D.AddForce(transform.forward * _boostForce, ForceMode2D.Impulse);
            _boostParticle.Play();
            _isReadyToBoost = false;
        }

        //if (false)
        //    LoopAround();

    }
/*
    void LoopAround()
    {
        if(transform.position.x > 9f)
            transform.position = new Vector3(-9f, transform.position.y, 0f);
        else if(transform.position.x < -9f)
            transform.position = new Vector3(9f, transform.position.y, 0f);

        if (transform.position.y > 5.4f)
            transform.position = new Vector3(transform.position.x, -5.4f, 0f);
        else if (transform.position.y < -5.4f)
            transform.position = new Vector3(transform.position.x, 5.4f, 0f);
    }
*/
    void HandleShooting()
    {

        float intensity = GetAxis(5);

        if (intensity > 0f)
        {
            if (_player.IsParticleMode)
                Boost();  //FireProjectile();
            else
            {
                TriggerGunRay(true, intensity);
                if (!_isShooting)
                {
                    _player.Audio.WaveAudio(_isShooting, intensity);
                }
            }
        }
        else
        {
            //if (!_player.IsParticleMode && _isShooting)
            TriggerGunRay(false);
            _isShooting = false;
            _player.Audio.WaveAudio(_isShooting, 0f);
        }
    }

    void HandleSwitching()
    {
        //add some growing missile effects when holding?
        if (GetButton(11))
        {
            Switch(true);
        }
        else if(GetButton(12))
        {
            Switch(false);
        }
    }


    void TriggerGunRay(bool isShooting, float intensity = 0f)
    {
        if (!_isShooting && isShooting)
        {
            _player.Audio.WaveAudio(isShooting, intensity);
            _gunRayParticle.Play();
            _gunRay.gameObject.SetActive(isShooting);
            _particles = new ParticleSystem.Particle[_gunRayParticle.main.maxParticles];
        }

        _isShooting = isShooting;

        if (isShooting)
        {
            _beamCollider.enabled = true;
            float updatedIntensity = minIntensity + (maxIntensity - minIntensity) * intensity;

            _player.Audio.AudioSource.pitch = intensity * 2f;

            Amplitude = 1f / updatedIntensity;
            Freq = Mathf.Sqrt(updatedIntensity * 3f);
            yPosMultiplier = updatedIntensity * 2f;
            
            Vector2 colpoint0 = new Vector2(0f, -.5f);
            Vector2 colpoint1 = new Vector2(-updatedIntensity / 5f - 0.25f / (updatedIntensity - .2f), updatedIntensity * 7f);
            Vector2 colpoint2 = new Vector2(updatedIntensity / 5f + 0.25f / (updatedIntensity - .2f), updatedIntensity * 7f);
            List<Vector2> pointsList = new List<Vector2>();
            pointsList.Add(colpoint0);
            pointsList.Add(colpoint1);
            pointsList.Add(colpoint2);
            Vector2[] points = pointsList.ToArray();
            _beamCollider.SetPath(0, points);            

            int particleCount = _gunRayParticle.GetParticles(_particles);
            for (int i = 0; i < particleCount; i++)
            {
                ParticleSystem.Particle currentParticle = _particles[i];       
                
                float yPos = i * (yPosMultiplier / 100f);
                float xPos = Mathf.Sin(yPos * Freq * 2 * Mathf.PI + Time.time * 10f * Mathf.Exp(intensity)) * Amplitude * ((1f - updatedIntensity) * yPos * expAmplitude);
                float zPos = 0f;

                Vector3 global = new Vector3(xPos, yPos, zPos);
                Vector3 absoluteVector = transform.TransformDirection(global);
                Vector3 local = transform.position + new Vector3(absoluteVector.x, absoluteVector.y, absoluteVector.z);

                _particles[i].position = local;
            }
            _gunRayParticle.SetParticles(_particles, particleCount);
        }
        else
        {
            _gunRayParticle.Stop();
            _beamCollider.enabled = false;
        }
    }

    public void Switch(bool isLeft)
    {
        _isShooting = false;
        TriggerGunRay(false);
        /*
        if (!_player.IsParticleMode)
        {
            TriggerGunRay(false);
        }
*/
        _isSwitching = true;
        LeanTween.rotateAroundLocal(_model, Vector3.up, isLeft ? 180f : -180f, _timeSwitch).setOnComplete(OnSwitchFinished);
        _player.Switch();
    }

    void OnSwitchFinished()
    {
        _isSwitching = false;
    }

    float GetAxis(int id)
    {
        if (_gamepad == null)
            return 0f;

        int[] axisValues = (int[])System.Enum.GetValues (typeof(GamepadAxis));
        return _gamepad.GetAxis ((GamepadAxis)axisValues [id]);
    }

    public bool GetButton(int id)
    {
        if (_gamepad == null)
            return false;

        int[] buttonValues = (int[])System.Enum.GetValues (typeof(GamepadButton));
        return _gamepad.GetButton ((GamepadButton)buttonValues [id]);
    }

    public bool GetButtonDown(int id)
    {
        if (_gamepad == null)
            return false;

        int[] buttonValues = (int[]) System.Enum.GetValues(typeof(GamepadButton));
        return _gamepad.GetButtonDown((GamepadButton) buttonValues[id]);
    }

    public bool IsShooting
    {
        get { return _isShooting; }
    }

    /*public GamePadDeadZone Gamepad
    {
        get { return _gamepad; }
    }*/

}
