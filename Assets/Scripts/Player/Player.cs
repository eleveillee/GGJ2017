﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using XInputDotNetPure;

public class Player : MonoBehaviour, IDamageable
{
    [SerializeField] private Slider _healthBar;
    [SerializeField] private Slider _shieldBar;        

    private TrailRenderer _trail;

    [SerializeField] private GameObject _explosionParticle;
                     private ParticleSystem _explosionPS;
    [SerializeField] private ParticleSystem _sphere; 
    private Renderer _renderer;
    private Color _sphereColor;
    private ParticleSystem.MainModule _sphereMain;
    private float _maxAlpha = .5f;

    public static int PointsToWin = 100;
    private int _winPoints = 0;

    private float _hitPoints;
    private float _maxHitPoints = 10f;
    private float _shieldPoints;
    private float _maxShieldPoints = 5f;

    private bool _isTakingDamage;

    private float _lastScoreTime;
    public int Id;

    private PlayerController _controller;
    private PlayerAudio _audio;
    public bool IsAlive;   

    public bool IsParticleMode;    
    //types of characters possible?

    void Awake()
    {
        _controller = GetComponent<PlayerController>();
        _audio = GetComponent<PlayerAudio>();
        _sphereMain = _sphere.main;
        _trail = GetComponent<TrailRenderer>();
        _renderer = GetComponentInChildren<MeshRenderer>();        
    }

    void Start()
    {
        Reset();
        _sphereColor = GetColor();
        _trail.startColor = GetColor();
        _trail.endColor = GetColor();

        _renderer.materials[3].color = GetColor();
        _renderer.materials[6].color = GetColor();
    }    

    void SetHealth(float value)
    {
        _hitPoints = value;
        _hitPoints = Mathf.Clamp(_hitPoints, 0f, _maxHitPoints);
        _healthBar.value = _hitPoints;        
    }

    void SetShieldAlpha(float alpha)
    {
        _sphereColor.a = alpha;
        _sphereColor.a = Mathf.Clamp(_sphereColor.a, 0f, _maxAlpha);
        _sphereMain.startColor = _sphereColor;        
        _trail.startColor = _sphereColor;
        _trail.endColor = _sphereColor;
    }

    void SetShieldValue(float value)
    {        
        _shieldPoints = value;
        _shieldPoints = Mathf.Clamp(_shieldPoints, 0f, _maxShieldPoints);
        _shieldBar.value = _shieldPoints;        
        float valueToAlpha = _maxShieldPoints / (_maxAlpha * 100f);
        SetShieldAlpha(value * valueToAlpha);
    }

    public void AddDamage(float damage)
    {
        Debug.Log("Taknig damage : " + damage);
        _isTakingDamage = true;
        //float iniHp = _hitPoints; // Debugging
        if (_shieldPoints > 0.1f)
        {
            SetShieldValue(_shieldPoints - damage);
        }
        else
        {
            //Debug.Log("AddDamage : " + iniHp + " -> " + _hitPoints + " damage :  " +  damage);  // Debugging
            float damageTaken = IsParticleMode ? damage * 5f : damage;
            _hitPoints -= damageTaken;
            _healthBar.value = _hitPoints;
        }

        StartCoroutine(VibrateController(0.1f, 1f));

        if (_hitPoints <= 0)
            Die();

        _isTakingDamage = false;
    }

    IEnumerator VibrateController(float time, float force)
    {/*
        GamePad.SetVibration((PlayerIndex)Id, force, force);
        yield return new WaitForSeconds(time);
        GamePad.SetVibration((PlayerIndex)Id, 0f, 0f);
*/
        yield return null; // Deactivated for tests
    }

    void RegenSheild()
    {
        if (_isTakingDamage || _controller.IsShooting || IsParticleMode)
            return;

        if (_shieldPoints <= _maxShieldPoints)
            SetShieldValue(_shieldPoints + .5f * Time.deltaTime);
    }

    public void Initialize(int id)
    {
        Reset(false);
        Id = id;
        _controller.Initialize();
    }

    public void Switch()
    {
        IsParticleMode = !IsParticleMode;

        if (IsParticleMode)
            SetShieldValue(0f);

    }

    void AddScore()
    {
        LevelManager.Instance.PlayerAddScore(Id, 1);
        _lastScoreTime = Time.time;
    }

    void Update()
    {
        if (_controller.IsShooting)
            SetShieldValue(_shieldPoints - 0.01f);

        else
            RegenSheild();

        /*if (IsParticleMode && Time.time - _lastScoreTime > _timeScoreWaveMode)
            AddScore();*/
    }

    void LateUpdate()
    {
        _healthBar.transform.rotation = Quaternion.identity;
        _healthBar.transform.position = transform.localPosition + Vector3.up * 0.6f;
        _shieldBar.transform.rotation = Quaternion.identity;
        _shieldBar.transform.position = transform.localPosition + Vector3.up * 0.7f;
    }

    void OnParticleCollision(GameObject other)
    {
        Debug.Log("Particle Collision : " + other);
    }

    void WinRound()
    {
        //Debug.Log(gameObject.name + "Won");
    }


    public void Die()
    {
        _audio.DieAudio();
        IsAlive = false;
        LevelManager.Instance.OnPlayerDied(this);
        gameObject.SetActive(false);
        LeanTween.delayedCall(2.5f, () => { Reset(); });
        VibrateController(0.3f, 0f);
        //LeanTween.delayedCall(0.3f, () => { GamePad.SetVibration((PlayerIndex)Id, 0f, 0f);});
        LevelManager.Instance.PlayerAddScore(Id, -1);
        if (_explosionPS == null)
        {
            GameObject exp = Instantiate(_explosionParticle);
            _explosionPS =  exp.GetComponent<ParticleSystem>();
            _explosionPS.Play();
        }
        else
        {
            _explosionPS.transform.position = this.transform.position;
            _explosionPS.Play();
        }
    }

    public void Reset(bool isResetController = true)
    {
        if(isResetController)
            _controller.Reset();

        SetHealth(_maxHitPoints);
        SetShieldValue(_maxShieldPoints);
        _healthBar.maxValue = _maxHitPoints;
        _shieldBar.maxValue = _maxShieldPoints;
        gameObject.SetActive(true);

        IsAlive = true;
    }

    void OnDestroy()
    {
        GamePad.SetVibration((PlayerIndex)Id, 0f, 0f);
    }

    private void OnApplicationQuit()
    {
        GamePad.SetVibration((PlayerIndex)Id, 0f, 0f);
    }

    Color GetColor()
    {
        return GameManager.Instance.GameDefinition._playerColors[Id];
    }

    public PlayerController Controller
    {
        get { return _controller; }
    }

    public PlayerAudio Audio
    {
        get { return _audio; }
        set { _audio = value; }
    }

    public void GetToken()
    {
        _audio.TokenAudio();
        AddScore();
    }
}
