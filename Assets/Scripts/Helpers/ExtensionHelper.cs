﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;

using Random = UnityEngine.Random;

public static class ExtensionHelper {

    // Action
    public static void SafeInvoke(this Action action)
    {
        if (action != null)
            action.Invoke();
    }

    // Vector2
    public static float GetRandomValue(this Vector2 vector)
    {
        return Random.Range(vector.x, vector.y);
    }

    // Text
    public static void SetValue(this UnityEngine.UI.Text text, float value, float duration = 0.0f)
    {
        text.text = String.Format("{0:0}", value);
    }

    // List
    public static T GetRandomValue<T>(this List<T> list)
    {
        return list[Random.Range(0, list.Count)];
    }
         
    public static void MoveItemAtIndexToFront<T>(this List<T> list, int index)
    {
        T item = list[index];
        for (int i = index; i > 0; i--)
            list[i] = list[i - 1];
        list[0] = item;
    }

    // Transform
    public static void SetY(this Transform transform, float value)
    {
        transform.localPosition = new Vector3(transform.localPosition.x, value, transform.localPosition.z);
    }

    // GameObject
    public static void ToggleActive(this GameObject go)
    {
        go.SetActive(!go.activeSelf);
    }

    /*public static void SetImageAlpha(this GameObject go, float alpha)
    {
        UnityEngine.UI.Image image = go.GetComponent<UnityEngine.UI.Image>();
        Color newColor = image.color;
        newColor.a = alpha;
        image.color = newColor;
    }*/

    // Camera
    public static Bounds OrthographicBounds(this Camera camera)
    {
        float screenAspect = (float)Screen.width / (float)Screen.height;
        float cameraHeight = camera.orthographicSize * 2;
        Bounds bounds = new Bounds(
            camera.transform.position,
            new Vector3(cameraHeight * screenAspect, cameraHeight, 0));
        return bounds;
    }

    //Rigidbody2D
    public static void AddExplosionForce(this Rigidbody2D body, float explosionForce, Vector3 explosionPosition, float explosionRadius)
    {
        var dir = (body.transform.position - explosionPosition);
        float wearoff = 1 - (dir.magnitude / explosionRadius);
        body.AddForce(dir.normalized * explosionForce * wearoff, ForceMode2D.Impulse);
    }

    public static void AddExplosionForce(this Rigidbody2D body, float explosionForce, Vector3 explosionPosition, float explosionRadius, float upliftModifier)
    {
        var dir = (body.transform.position - explosionPosition);
        float wearoff = 1 - (dir.magnitude / explosionRadius);
        Vector3 baseForce = dir.normalized * explosionForce * wearoff;
        body.AddForce(baseForce);

        float upliftWearoff = 1 - upliftModifier / explosionRadius;
        Vector3 upliftForce = Vector2.up * explosionForce * upliftWearoff;
        body.AddForce(upliftForce);
    }

}
