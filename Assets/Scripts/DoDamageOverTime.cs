﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoDamageOverTime : MonoBehaviour
{
    //private bool _isDamaging;

    [SerializeField]
    private float _damagePerTick = 0.1f;

    [SerializeField]
    private float _tickDeltaTime = 0.1f;

    private IDamageable _target;
    private float _timeLastTick;

    void OnTriggerStay2D(Collider2D other)
    {
        TestTarget(other.gameObject);
    }

    private void OnCollisionStay2D(Collision2D other)
    {
        TestTarget(other.gameObject);
    }

    void TestTarget(GameObject other)
    {
        _target = other.GetComponent<IDamageable>();
        if (_target == null)
            return;


        if (Time.time > _timeLastTick + _tickDeltaTime)
        {
            _target.AddDamage(_damagePerTick);
        }
    }
}
