﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;

    [Header("UI Panels and overlays")]
    //[SerializeField] UIPanelMainMenu _uiPanelMainMenu;
    [SerializeField] UIOverlay _uiOverlay;
    [SerializeField] UIPanelMainMenu _uiPanelMainMenu;
    [SerializeField] UIPanelPause _uiPanelPause;
    [SerializeField] UIPanelDebug _uiPanelDebug;
    [SerializeField] UIPanelMidGame _uiPanelMidGame;
    [SerializeField] UIPanelEndGame _uiPanelEndGame;

    Dictionary<PanelType, UIPanel> _panels = new Dictionary<PanelType, UIPanel>();

    [SerializeField] private CanvasGroup _canvasGroup;

    public PanelType CurrentMenuType;
    // Use this for initialization
    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

        _panels.Add(PanelType.MainMenu, _uiPanelMainMenu);
        _panels.Add(PanelType.Pause, _uiPanelPause);
        _panels.Add(PanelType.Debug, _uiPanelDebug);
        _panels.Add(PanelType.MidGame, _uiPanelMidGame);
        _panels.Add(PanelType.EndGame, _uiPanelEndGame);

        CloseAllPanels();
    }

    // Panels
    public void CloseAllPanels()
    {
        foreach(KeyValuePair<PanelType, UIPanel> pair in _panels)
        {
            pair.Value.Close();
        }
    }

    public UIPanel ShowPanel(PanelType type, bool isVisible = true)
    {
        CloseAllPanels();
        if (isVisible)
        {
            _panels[type].Open();
            CurrentMenuType = type;
        }   
        else
        {
            _panels[type].Close();
            CurrentMenuType = PanelType.Null;
        }

        return _panels[type];
    }

    // Overlays
    public void ShowUIOVerlay(bool isVisible = true)
    {
        _uiOverlay.Show(isVisible);
    }

    public void FadeIn(float time,  Action onCallback = null)
    {
        if(onCallback == null)
            LeanTween.alphaCanvas(_canvasGroup, 1f, time);
        else
            LeanTween.alphaCanvas(_canvasGroup, 1f, time).setOnComplete(onCallback);
    }

    public void FadeOut(float time, Action onCallback = null)
    {
        if(onCallback == null)
            LeanTween.alphaCanvas(_canvasGroup, 0f, time);
        else
            LeanTween.alphaCanvas(_canvasGroup, 0f, time).setOnComplete(onCallback);
    }
}

public enum PanelType
{
    Null,
    MainMenu,
    Pause,
    Debug,
    MidGame,
    EndGame
}