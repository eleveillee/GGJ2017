﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIPanelMainMenu : UIPanel
{
    protected override void OnOpen()
    {
    }

    protected override void OnClose()
    {
    }

    void Update()
    {
        if (!_isOpen) 
            return;


        if (LevelManager.Instance.IsPlayerPressingStart())
            OnPlay_Button();
    }

    public void OnPlay_Button()
    {
        FSM_GameState.Instance.ChangeState(FSM_GameState.Instance.State_Game);
    }
}