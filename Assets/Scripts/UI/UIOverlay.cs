﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class UIOverlay : MonoBehaviour
{
    [SerializeField] private List<GameObject> _playerScoreRoots = new List<GameObject>();
    [SerializeField] private List<Text> _playerScores = new List<Text>();
    [SerializeField] private List<Image> _playerMedals = new List<Image>();
    [SerializeField] private Sprite _goldMedal;
    [SerializeField] private Sprite _silverMedal;
    [SerializeField] private Sprite _bronzeMedal;
    [SerializeField] private Text _timer;

    private float _startingTime;
    private float _countDown;
    private bool _isVisible;
    private Color _initialColor;
    private Color _finalColor = new Color(255,167,107,1f);
    private bool _isTimerRunning = false;
    // Update is called once per frame
    void OnEnable()
    {
        _initialColor = _timer.color;

        for (int i = 0; i < 4; i++)
        {
            if (i < LevelManager.Instance.NumberOfPlayer)
            {
                _playerScoreRoots[i].gameObject.SetActive(true);
                _playerScores[i].color = GameManager.Instance.GameDefinition._playerColors[i];
            }
            else
            {
                _playerScoreRoots[i].gameObject.SetActive(false);
            }
        }
    }

	void Update ()
	{
	    if (!_isVisible || !LevelManager.Instance.IsInitialized)
	        return;

	    for(int i = 0; i < LevelManager.Instance.NumberOfPlayer; i++)
	    {
	        _playerScores[i].text = LevelManager.Instance.PlayerScores[i].ToString();
	    }

	    //var sortedDict = from entry in _playerScores orderby entry.Value ascending select entry;
	    List<KeyValuePair<int, int>> orderedScores = LevelManager.Instance.PlayerScores.OrderByDescending(x => x.Value).ToList();
	    _playerMedals[orderedScores[0].Key].gameObject.SetActive( orderedScores[0].Value > 0);
	    _playerMedals[orderedScores[0].Key].sprite = _goldMedal;

	    if (orderedScores.Count > 1)
	    {
	        _playerMedals[orderedScores[1].Key].gameObject.SetActive(orderedScores[1].Value > 0);
	        _playerMedals[orderedScores[1].Key].sprite = _silverMedal;
	    }
	    if (orderedScores.Count > 2)
	    {
	        _playerMedals[orderedScores[2].Key].gameObject.SetActive(orderedScores[2].Value > 0);
	        _playerMedals[orderedScores[2].Key].sprite = _bronzeMedal;
	    }

	    if (orderedScores.Count > 3)
	    {
	        _playerMedals[orderedScores[3].Key].gameObject.SetActive(false);
	        _playerMedals[orderedScores[3].Key].sprite = null;
	    }

	    if (_isTimerRunning)
	    {
	        float _timeLeft = (_countDown - (Time.time + _startingTime));
	        _timer.text = ((int)_timeLeft).ToString();
	        if (_timeLeft < 5f)
	        {
	            LeanTween.textColor(_timer.rectTransform, _finalColor, 0.1f).setOnComplete(() =>
	            {
	                LeanTween.textColor(_timer.rectTransform, _initialColor, 0.5f);
	            });

	            LeanTween.scale(_timer.gameObject, 1.3f * Vector3.one, 0.1f).setOnComplete(() =>
	            {
	                LeanTween.scale(_timer.gameObject.gameObject, Vector3.one, 0.5f);
	            });
	        }
	        if (_timeLeft <= 0f)
	        {
	            _isTimerRunning = false;
	            LevelManager.Instance.OnGameFinish();
	        }
	    }
	}

    public void StartTimer(float time)
    {
        Debug.Log("Time : " + time);
        _isTimerRunning = true;
        _countDown = time;
        _startingTime = Time.time;
    }

    public void Show(bool isVisible)
    {
        gameObject.SetActive(isVisible);
        _isVisible = isVisible;
    }

    public void TweenScore(int Id, int value)
    {
        LeanTween.scale(_playerScores[Id].gameObject, 1.25f * Vector3.one, 0.25f).setOnComplete(() =>
        {
            LeanTween.scale(_playerScores[Id].gameObject, Vector3.one, 0.25f);
        });
    }
}
