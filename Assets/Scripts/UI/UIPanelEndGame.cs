﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIPanelEndGame : UIPanel
{
    protected override void OnOpen()
    {
    }

    protected override void OnClose()
    {
    }

    void Update()
    {
        if (!_isOpen) 
            return;


        if (LevelManager.Instance.IsPlayerPressingStart())
            OnReset_Button();
    }

    public void OnReset_Button()
    {
        FSM_GameState.Instance.ChangeState(FSM_GameState.Instance.State_Game);
    }

    public void OnQuit_Button()
    {
        FSM_GameState.Instance.ChangeState(FSM_GameState.Instance.State_MainMenu);
    }
}