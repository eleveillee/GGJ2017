﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIPanelMidGame : UIPanel
{
    protected override void OnOpen()
    {
    }

    protected override void OnClose()
    {
    }

    void Update()
    {
        if (!_isOpen) 
            return;


        if (LevelManager.Instance.IsPlayerPressingStart())
            OnContinue_button();
    }

    public void OnContinue_button()
    {
        FSM_FightState.Instance.ChangeState(FSM_FightState.Instance.InProgress);
    }
}