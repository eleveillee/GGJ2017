﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hazard : MonoBehaviour, IDamageable
{
    private float _timeLife = 30f;

    [SerializeField]
    private float _damage;

    [SerializeField]
    private float _hp;

    private float _timeStart;
	
	void Start ()
	{
	    _timeStart = Time.time;
	}	
	
	void Update ()
	{
	    if (Time.time > _timeStart + _timeLife)
	        Die();
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        IDamageable iDamageable = (IDamageable)(other.gameObject.GetComponent<IDamageable>());

        if (iDamageable != null)
            iDamageable.AddDamage(_damage);
    }

    public void AddDamage(float damage)
    {
        _hp -= damage;
        if(_hp <= 0)
            Die();
    }

    public void Die()
    {
        Destroy(gameObject);
        Debug.Log("BlowUp");
    }
}
