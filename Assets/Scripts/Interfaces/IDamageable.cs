﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

public interface IDamageable
{
    void AddDamage(float damage);
    void Die();
}
