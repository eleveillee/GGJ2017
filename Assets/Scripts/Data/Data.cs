﻿using System;
using UnityEngine;

public class Data
{
    [SerializeField]
    protected string _id;

    [SerializeField]
    protected string _name;

    public string Id
    {
        get { return _id; }
        set { _id = value; }
    }

    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }

    public Data()
    {
        _id = Guid.NewGuid().ToString(); // On construction, a new GUID will be set, 
        _name = "ConfigNoName";
    } 
}
