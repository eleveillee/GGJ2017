﻿using System;
using UnityEngine;

public class DataScriptable : ScriptableObject
{
    [SerializeField]
    private string _id;

    public string Id
    {
        get { return _id; }
        set { _id = value; }
    }
    
    public void Awake()
    {
        _id = Guid.NewGuid().ToString();
    } 
}
