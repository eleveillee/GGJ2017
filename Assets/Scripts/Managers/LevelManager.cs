﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using System.Linq;
using System.Runtime.InteropServices;
using UnityEngine.Assertions.Must;
using XInputDotNetPure;
using Random = UnityEngine.Random;

public class LevelManager : MonoBehaviour
{
    // Singleton Instance
    public static LevelManager Instance;

    // Serialized Fields
    [SerializeField] private UIOverlay _uiOverlay;
    [SerializeField] private Player _playerPrefab;
    [SerializeField] private GameObject _worldRoot;
    public int NumberOfPlayer = 2;
    private float _delayRespawn = 2f;

    private float _numberRoundMax = 3;
    private float _numberRoundWon;

    [NonSerialized]
    public bool IsInitialized;

    [NonSerialized]
    public List<Player> Players = new List<Player>();

    public Dictionary<int, int> PlayerScores = new Dictionary<int, int>();

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
            
        //_levelTracker = GetComponentInChildren<LevelTracker>();
    }

    public void EnterGameMode()
    {
        if (!IsInitialized)
            InitializeGame();

        _worldRoot.SetActive(true);
        SpawnPlayers();
        WorldManager.Instance.StartWorld();
    }

    public void StartFight()
    {
        foreach (Player player in Players)
        {
            player.Reset();
        }
        TogglePlayerControllers(true);
        _uiOverlay.StartTimer(60f);
    }

    public void TogglePlayerControllers(bool canControl)
    {
        foreach (Player player in Players)
        {
            player.Controller.CanControl = canControl;
        }
    }

    public void ExitGameMode()
    {
        IsInitialized = false;
        _worldRoot.SetActive(false);
        for (int i = Players.Count - 1; i >= 0; i--)
            Destroy(Players[i].gameObject);

        WorldManager.Instance.StopWorld();

        Players.Clear();
        PlayerScores.Clear();
    }

    void SpawnPlayers()
    {
        for (int i = 0; i < NumberOfPlayer; i++)
        {
            Vector2 position = Vector2.zero;
            if (i == 0 && NumberOfPlayer == 1)
                position = new Vector2( 0f, 0f);
            else if(i == 0)
                position = new Vector2(-5f, 3.5f);
            else if (i == 1)
                position = new Vector2( 5f, 3.5f);
            else if (i == 2)
                position = new Vector2( 5f, -3.5f);
            else if (i == 3)
                position = new Vector2(-5f, -3.5f);

            SpawnPlayer(i, position);
        }
    }

    void InitializeGame()
    {
        for (int i = 0; i < NumberOfPlayer; i++)
        {
            PlayerScores.Add(i, 0);
        }
        IsInitialized = true;
    }

    void SpawnPlayer(int id, Vector2 position)
    {
        Player player = ((Player)Instantiate(_playerPrefab, position, Quaternion.identity, _worldRoot.transform));
        player.Initialize(id);
        player.Controller.CanControl = false;
        Players.Add(player);
    }

    public void RespawnPlayer(Player player)
    {
        StartCoroutine(Respawn(player));
    }

    IEnumerator Respawn(Player player)
    {
        player.gameObject.SetActive(false);
        yield return new WaitForSeconds(_delayRespawn);
        player.gameObject.SetActive(true);
    }

    public void PlayerAddScore(int Id, int value)
    {
        PlayerScores[Id] += value; // Add 1 point
        if (PlayerScores[Id] < 0)
            PlayerScores[Id] = 0;
        _uiOverlay.TweenScore(Id, value);
    }

    public void OnPlayerDied(Player player)
    {
        /*List<Player> playersAlive = Players.FindAll(x => x.IsAlive).ToList();
        if(playersAlive.Count == 1)
            OnPlayerWin(playersAlive[0]);*/
    }

    public void OnGameFinish() // Reference : https://en.wikipedia.org/wiki/Big_Rigs:_Over_the_Road_Racing
    {
        _numberRoundWon++;
        if(_numberRoundWon > _numberRoundMax)
            FSM_FightState.Instance.ChangeState(FSM_FightState.Instance.EndGame);
        else
            FSM_FightState.Instance.ChangeState(FSM_FightState.Instance.MidGame);
    }

    public bool IsPlayerPressingStart()
    {
        bool isPressStart = false;

        int[] buttonValues = (int[])System.Enum.GetValues (typeof(GamepadButton));
        foreach (GamepadDevice gamepad in GamepadInput.Instance.gamepads)
        {
            isPressStart = isPressStart || gamepad.GetButtonDown ((GamepadButton)buttonValues [5]);
        }

        return isPressStart;
    }

    /*void OnLastRoundCompleted() // Reference : https://en.wikipedia.org/wiki/Big_Rigs:_Over_the_Road_Racing
    {
        FSM_FightState.Instance.ChangeState(FSM_FightState.Instance.EndGame);
    }*/
/*
    public LevelTracker Tracker
    {
        get { return _levelTracker; }
        set { _levelTracker = value; }
    }

    public Player Player
    {
        get { return _player; }
        set { _player = value; }
    }

    public GameObject PlayerPrefab
    {
        get { return _playerPrefab; }
    }

    public DataEncounter CurrentEncounter
    {
        get { return _currentEncounter; }
    }
    */
}