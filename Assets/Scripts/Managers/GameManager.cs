﻿using UnityEngine;
using System.Collections;
using System;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    private PlayerProfile _playerProfile;

    [SerializeField]
    private AssetGameDefinition _gameDefinition;

    [SerializeField]
    private bool isStartStraightInGame = true;

    public Action<PlayerProfile> OnNewPlayerProfile;
    // Use this for initialization
    void Awake ()
	{
	    if (Instance == null)
	        Instance = this;
        else
            Destroy(gameObject);
    }

    void Start()
    {
        if(isStartStraightInGame)
            FSM_GameState.Instance.ChangeState(FSM_GameState.Instance.State_Game);
        else
            FSM_GameState.Instance.ChangeState(FSM_GameState.Instance.State_Boostrap);
    }

    public void InitializePlayerInstances()
    {

    }

    public AssetGameDefinition GameDefinition
    {
        get { return _gameDefinition; }
    }
}
