﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Token : MonoBehaviour
{
    private bool _isConsumed = false;

	// Use this for initialization
	void Start () {
	    LeanTween.scale(gameObject, 1.5f * Vector3.one, 0.1f)
	        .setOnComplete(() =>
	        {
	            LeanTween.scale(gameObject, Vector3.one, 0.15f);//.setOnComplete(() => { Destroy(gameObject); });
	        });
	}

	// Update is called once per frame
    private void OnTriggerStay2D(Collider2D other)
    {
        Player player = other.GetComponent<Player>();
        if (_isConsumed == true || player == null || player.IsParticleMode == false)
            return;

        player.GetToken();
        _isConsumed = true;

        // Tween amd destroy
        LeanTween.scale(gameObject, 1.25f * Vector3.one, 0.2f)
            .setOnComplete(() =>
            {
                LeanTween.scale(gameObject, Vector3.zero, 0.3f).setOnComplete(() => { Destroy(gameObject);
                    WorldManager.Instance.TokenDestroyed();
                });
            });
    }
}
