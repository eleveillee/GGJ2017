﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    private float _speed = 1f;
    private float _contactDamage = 1f;
    private float _explosionDamage = 5f;
    private float _startingTime;
    private float _lifetime;

    private float scaling = 0.1f;
    private float maxScale = 4f;
    private float _speedScaling = 1f;

    private float _constantForce = 100f;
    private float _explosiveForce = 1000f;

    private Rigidbody2D _rigidbody2D;

    void Start()
    {
        _startingTime = Time.realtimeSinceStartup;
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _startingTime = Time.time;
    }

    /*void OnTriggerEnter2D(Collider2D other)
    {
        IDamageable iDamageable = (IDamageable)(other.GetComponent<IDamageable>());

        if (iDamageable != null)
            iDamageable.AddDamage(_dmgPoints);
    }*/

    private void OnCollisionEnter2D(Collision2D other)
    {
        IDamageable iDamageable = (IDamageable)(other.gameObject.GetComponent<IDamageable>());

        if (iDamageable != null)
            iDamageable.AddDamage(_contactDamage);
    }

    void FixedUpdate()
    {
        _rigidbody2D.AddForce(_rigidbody2D.velocity.normalized * _constantForce, ForceMode2D.Force);
        if (transform.localScale.x < maxScale)
        {
            transform.localScale = new Vector3(transform.localScale.x + scaling, transform.localScale.y + scaling,
            transform.localScale.z + scaling);
        }
        else
        {
            Explode();
        }

        /*if (Time.time > _startingTime + _lifetime)
        {

        }*/
    }

    void Explode()
    {
        Vector3 explosionPos = transform.position;
        float radius = maxScale;

        Collider2D[] colliders = Physics2D.OverlapCircleAll(explosionPos, radius);
        foreach (Collider2D hit in colliders) {
            Rigidbody2D rb2d = hit.GetComponent<Rigidbody2D>();

            if (rb2d != null)
            {
                rb2d.AddExplosionForce( _explosiveForce, explosionPos, radius);
            }
        }

        Destroy(gameObject);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawSphere(transform.position, maxScale);
        Gizmos.color = new Color(1f,0f,0f,0.5f);
    }
}
