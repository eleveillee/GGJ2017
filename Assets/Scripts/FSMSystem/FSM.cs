﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FSM
{
    public abstract class FSM : MonoBehaviour
    {

        List<State> _states = new List<State>();

        public State CurrentState { get; private set; }

        /*public void Start()
        {
            if(_initialState != null)
                ChangeState(_initialState);
            //StartCoroutine(ExecuteQueue());
        }*/

        public virtual IEnumerator ExecuteQueue()
        {
            foreach(State state in _states)
            {
                ChangeState(state);
                /*if(state.IsInstant)
                {
                    state.Exit();
                }
                else
                {
                    //yield return new wait;
                }*/
            }

            yield return null;
        }

        void Update()
        {
            if (CurrentState != null && CurrentState.IsInitialized)
                CurrentState.Update();

        }

        protected void AddState(State state)
        {
            _states.Add(state);
            state.Initialize(this);
        }

        public void ChangeState(State state)
        {
            //Debug.Log("New State : " + state);

            if(CurrentState != null)
                CurrentState.Exit();

            CurrentState = state;
            CurrentState.Initialize(this);
            CurrentState.Enter();
        }
    }
}


