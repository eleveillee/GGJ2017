﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private float _minDistance = 15f;
    private float _maxDistance = 22f;
    private float _maxSize = 15f;
    private float _maxX = 20f;
    private float _maxY = 11f;

    private Camera _camera;
    float _dankingParam = 3.0f;
    private float cameraResizeSpeed = 2f;

    void Awake()
    {
        _camera = GetComponent<Camera>();
    }

	// Update is called once per frame
	void LateUpdate ()
	{
	    if (!LevelManager.Instance.IsInitialized)
	        return;


	    Bounds playerBounds = new Bounds();
	    int i = 0;
	    foreach (Player player in LevelManager.Instance.Players)
	    {
	        if(i == 0)
	        {
                playerBounds = new Bounds(player.transform.position, Vector3.one);
	        }
	        else
	        {
	            playerBounds.Encapsulate(player.transform.position);
	        }
	        i++;
	    }

	    ResizeCamera(playerBounds);
	    MoveToTarget(playerBounds.center);
	}

    void ResizeCamera(Bounds playerBounds)
    {
        //Debug.Log(playerBounds.size.x + " vs " + _camera.OrthographicBounds().size.x);

        // Evaluate camera bounds at player distance
        Bounds bounds = GetCameraBounds(transform.position);

        float _ratioSpeed = 0f;
        float _upperRatio = 0.6f;
        float _lowerRatio = 0.8f;
        if (playerBounds.size.x > _upperRatio * bounds.size.x || (playerBounds.size.y > _upperRatio * bounds.size.y))
        {
            _ratioSpeed = Mathf.Max((playerBounds.size.x - _upperRatio * bounds.size.x),
                (playerBounds.size.y - _upperRatio * bounds.size.y));

           SetDistance(_ratioSpeed);
        }
        else if (playerBounds.size.x < _lowerRatio * bounds.size.x || playerBounds.size.y < _lowerRatio * bounds.size.y)
        {
            _ratioSpeed = Mathf.Max((playerBounds.size.x - _lowerRatio * bounds.size.x),
                (playerBounds.size.y - _lowerRatio * bounds.size.y));

           SetDistance(_ratioSpeed);
        }
    }

    Bounds GetCameraBounds(Vector3 position)
    {
        Vector3 v3ViewPort = new Vector3(0,0, -position.z);
        Vector3 v3BottomLeft = Camera.main.ViewportToWorldPoint(v3ViewPort);

        v3ViewPort.Set(1,1, -position.z);
        Vector3 v3TopRight = Camera.main.ViewportToWorldPoint(v3ViewPort);

        Bounds bounds = new Bounds();
        bounds.Encapsulate(v3BottomLeft);
        bounds.Encapsulate(v3TopRight);

        return bounds;
    }

    void SetDistance(float ratioSpeed)
    {
        float newPositionZ = transform.position.z - cameraResizeSpeed * Time.deltaTime * ratioSpeed;
        _camera.transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Clamp(newPositionZ, -_maxDistance, -_minDistance));
        //Debug.Log("Set " + ratioSpeed);

    }

    void MoveToTarget(Vector3 target)
    {
        Vector3 updatedTarget = new Vector3(target.x, target.y, transform.position.z);
        Vector2 newPosition = Vector2.Lerp(transform.position, updatedTarget , _dankingParam * Time.deltaTime); //+ _offset
/*
        Bounds boundsNewPos = GetCameraBounds(newPosition);
        Debug.Log(boundsNewPos.min);
        if (transform.position.x + boundsNewPos.min.x < -_maxX || transform.position.x + boundsNewPos.max.x > _maxX)
            newPosition.x = transform.position.x;

        if (transform.position.y + boundsNewPos.min.y < -_maxY || transform.position.y + boundsNewPos.max.y > _maxY)
            newPosition.y = transform.position.y;
*/
        transform.position = new Vector3(Mathf.Clamp(newPosition.x, -_maxX, _maxX), Mathf.Clamp(newPosition.y, -_maxY, _maxY), transform.position.z);
        //transform.position = new Vector3(newPosition.x, newPosition.y, transform.position.z);
    }
}
